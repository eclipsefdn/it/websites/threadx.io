---
title: "Frequently Asked Questions"
date: 2024-11-16T10:00:00-04:00
headline: "Frequently Asked Questions"
hide_sidebar: true
hide_page_title: true
---

Last Updated: November 16, 2024

{{< table_of_contents >}}

---

### What is Eclipse ThreadX®?
Eclipse ThreadX is the world’s first and only permissively licensed open source embedded real-time operating system (RTOS) certified for safety-critical applications governed under the stewardship of a vendor-neutral foundation.

ThreadX was previously owned by Microsoft and marketed under the Azure RTOS name. Microsoft contributed Azure RTOS and the ThreadX trademark to the Eclipse Foundation in November 2023. You can learn more in the [announcement FAQ](/announcement-faq).

---

### What is an RTOS?

An RTOS, or Real-Time Operating System, is tailored to meet the distinctive needs of real-time systems spanning various domains, including consumer electronics, embedded systems, Internet of Things (IoT) devices, industrial control systems, automotive systems, avionics, medical devices, and more. An RTOS is characterized by its deterministic behavior, ensuring predictable and consistent task execution times. Efficient task scheduling, low-latency responses to external events, and robust interrupt handling are fundamental features that distinguish an RTOS. Additionally, effective communication and synchronization mechanisms, along with resource management, contribute to the reliability and precision required in real-time systems.

---

### What components/technologies are part of ThreadX?

- ThreadX - an advanced real-time operating system (RTOS) designed specifically for deeply embedded applications. ThreadX also supports Symmetric Multi-Processing (SMP) through ThreadX SMP.
- NetX Duo - an advanced, industrial-grade TCP/IP network stack designed specifically for deeply embedded real-time and IoT applications. It also includes a MQTT client.
- FileX - a high-performance, FAT-compatible file system that’s fully integrated with ThreadX kernel.
- GUIX - a complete, embedded graphical user interface (GUI) library.
- USBX - a high-performance USB host, device, and on-the-go (OTG) embedded stack, that is fully integrated with ThreadX kernel.
- LevelX - Flash Wear Leveling for FileX and stand-alone purposes.
- GuiX Studio - a design environment facilitating the creation and maintenance of all graphical elements for GUIX.
- TraceX - an analysis tool that provides a graphical view of real-time system events to better understand the behavior of real-time systems.

---

### Who are the stakeholders and participants in this initiative?
Organisations willing to support the continued growth and sustainability of the Eclipse ThreadX platform and its ecosystem, driving the advancement of next-generation embedded and safety-critical solutions, are grouped into the ThreadX Alliance(https://threadxalliance.org).

ThreadX also attracted the interest of several automotive players. Those participate in the [ThreadX special interest group (SIG)](https://sdv.eclipse.org/special-interest-groups/threadx/) inside the Eclipse Software Defined Vehicle (SDV) working group. 

Finally, the Eclipse Foundation has a number of its members [offering services and support for Eclipse ThreadX](/services-and-support). 

---

### How is this contribution impacting the open source community and the embedded RTOS industry as a whole?

ThreadX is the world’s first open source real time operating system (RTOS) with multiple safety certifications governed under the stewardship of a vendor-neutral foundation. It provides a vendor-neutral, open source, safety certified OS for real-time applications published under a permissive license. ThreadX is the first and only RTOS in the market that has all four of those attributes and comes with a complete embedded development suite.

---

### What is the license for ThreadX?

ThreadX is available under the [MIT license](https://opensource.org/license/mit), a well-known and highly regarded permissive open source license. This makes adoption of and contribution to ThreadX as simple as possible for all parties.

---

### Which safety standards is ThreadX certified for?

ThreadX is certified for use in safety-critical applications, including home appliances, road vehicles, medical devices, and trains. 

The following standards have been used for certification:
- IEC 61508-3:2010; clause 7.4.2.12, route 3S
- IEC 62304:2015
- ISO 26262-8:2018; clause 12
- EN 50128:2011; clause 7.3.4.7

The certification has been performed by [SGS-TÜV Saar](https://www.sgs-tuev-saar.com/) 

---

### I want to build a safety-certified product or device using ThreadX. What should I do?

Specific versions of Eclipse ThreadX® have been certified for safety-critical applications. You will find information about each of the certified on the [ThreadX Alliance website](https://threadxalliance.org/subscription/safety-certifications). 

If you intend to certify your product or device, you can only use the component versions for which a certificate is available.

Subscribers of the ThreadX Alliance can purchase a license for the ThreadX safety artefacts package, which contains a full set of safety manuals and other documents for every certified ThreadX release.

---

### What does safety-certified OS mean?

It means the OS has been validated for conformance against specific international and industry standards by third-party auditors. 

ThreadX and its components have been tested according to the following standards:
- IEC 61508-3:2010; clause 7.4.2.12, route 3S
- IEC 62304:2015
- ISO 26262-8:2018; clause 12
- EN 50128:2011; clause 7.3.4.7

---

### How does ThreadX fit into the broader Embedded, IoT and Edge Computing ecosystem at the Eclipse Foundation?

ThreadX will is a project under the Eclipse IoT top-level project and the guidance of its Project Management Committee (PMC). As such, the ThreadX open source project is joining a large and well-established open source community.

The Eclipse IoT Working Group is home to the most extensive open source IoT and edge computing toolkit in the industry. The project portfolio encompasses the entire device-to-edge-to-cloud continuum.This software portfolio targets the three pillars of IoT and Edge: constrained devices, edge nodes, and IoT Cloud platforms.

Eclipse ThreadX reinforces the Eclipse IoT and edge portfolio in a significant way. It brings to the table a mature, high-performance, small footprint real time operating system (RTOS) that other Eclipse projects can leverage. This also opens up the use and adoption of other Eclipse IoT technologies, such as Sparkplug and zenoh, to ThreadX developers.

The Eclipse ThreadX safety and security certifications also make it a very attractive platform for software-defined vehicles (SDV). Eclipse SDV projects targeting the in-car environment will undoubtedly benefit from the addition of ThreadX to the Eclipse portfolio.

---

### How will the Eclipse Foundation and its community manage, maintain and support TheadX going forward?

The Eclipse Foundation established the [ThreadX Alliance](https://threadxalliance.org) as a structure to support the development and adoption of ThreadX. The Foundation’s vendor-neutral model of governance will ensure that all stakeholders of the ThreadX ecosystem can participate and make their voices heard.

Developers willing to contribute fixes and new features to the Eclipse ThreadX open source project can do so even if their employers are not members of the Eclipse Foundation or the ThreadX Alliance. Anyone can become a project contributor by creating an Eclipse Foundation account and signing the [Eclipse Contributor Agreement](https://www.eclipse.org/legal/eca/) electronically. Just make sure the email address referenced by the account is the same you intend to use when you commit to Git.

---

### In this new scenario, what are some of the advantages of using ThreadX over other embedded RTOS options?

Since it is available under the permissive MIT license, you can use ThreadX without licensing or royalty fees. 

If your product requires safety certification, ThreadX represents a mature, proven, yet affordable alternative to commercial RTOSes.

---

### Are there any specific use cases or applications the foundation envisions for ThreadX?

Eclipse ThreadX is a good choice for any type of embedded, IoT, or industrial automation application where a real-time operating system is needed. The impressive list of ThreadX safety and security certifications (e.g. IEC 61508, IEC 62304, ISO 26262, EN 50128) make it an attractive choice for applications and use cases requiring a high level of dependability.

While the suitability of any product for use in a specific application or use case needs to be determined by the implementer, these certifications are designed to meet the varying needs of a broad range of industry applications:

#### **IEC 61508**: Functional Safety of Electrical/Electronic/Programmable Electronic Safety-Related Systems

**Applications**: IEC 61508 is applicable to multiple industries, including:

- Chemical and processing
- Oil and gas
- Power generation and distribution
- Rail transportation
- Automotive
- Aerospace

#### **IEC 62304**: Medical Device Software - Software Life Cycle Processes

**Applications**: IEC 62304 is specifically designed for the software development life cycle of medical device software. It is applicable to:

- Healthcare and medical devices
- Diagnostic equipment
- Monitoring devices
- Therapeutic devices
- Health information technology

#### **ISO 26262**: Road Vehicles - Functional Safety

**Applications**: ISO 26262 is tailored for the automotive industry:

- Automotive electronic systems
- Advanced driver-assistance systems (ADAS)
- Engine control systems
- Electric and hybrid vehicles
- Autonomous vehicles

#### **EN 50128**: Railway Applications - Communications, Signaling, and Processing Systems

**Applications**: EN 50128 is specific to the railway industry:

- Train control and management systems
- Signaling systems
- Communication systems in railways
- Processing systems used in railways

---

### How can developers and organizations get involved with or utilize the newly open sourced ThreadX technology?

Like other Eclipse Foundation projects, Eclipse ThreadX is open, transparent, and meritocratic. The GitHub repositories are public. Anyone can contribute changes to the code or submit issues. Naturally, the permissive MIT license used means adopters can freely use the technology in their products.

The Eclipse ThreadX project has a public mailing list. Here are some useful links:

- [Contact us](mailto:threadx-info@eclipse.org) about joining the [ThreadX Alliance](https://threadxalliance.org)
- Contribute to the [project](https://projects.eclipse.org/projects/iot/iot.threadx) and join the [Eclipse ThreadX developer mailing list](https://accounts.eclipse.org/mailing-list/threadx-dev)
- Stay connected by joining the general [Eclipse ThreadX mailing list](https://accounts.eclipse.org/mailing-list/threadx)

---
